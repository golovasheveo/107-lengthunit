package telran.measure;

public class Length {
    private float      number;
    private LengthUnit unit;

    public Length ( float number, LengthUnit unit ) {
        this.number = number;
        this.unit   = unit;
    }

    public Length plus ( Length length ) {
        length = length.convert ( this.unit );
        return new Length ( this.number + length.getNumber ( ), this.unit );
    }

    public Length minus ( Length length ) {
        length = length.convert ( this.unit );
        return new Length ( this.number - length.getNumber ( ), this.unit );
    }

    public Length convert ( LengthUnit unit ) {
        return new Length ( this.number * ( this.unit.getValue ( ) / unit.getValue ( ) ), unit );
    }

    @Override
    public boolean equals ( Object obj ) {
        if ( this == obj ) return true;
        if ( obj == null || getClass ( ) != obj.getClass ( ) ) return false;


        return ( Float.hashCode ( number ) == Float.hashCode ( ((Length) obj).number ))
                && (unit == ((Length) obj).unit);
    }

    public float getNumber ( ) {
        return this.number;
    }

    public LengthUnit getUnit ( ) {
        return this.unit;
    }

    public void setNubmer ( float number ) {
        this.number = number;
    }

    public void setUnit ( LengthUnit unit ) {
        this.unit = unit;
    }

    @Override
    public String toString ( ) {
        return this.number + this.unit.toString ( );
    }

}