package telran.measure;

public enum LengthUnit {
    MM ( 1f ), CM ( 10f ), IN ( 25.4f ), FT ( 304.8f ), M ( 1000f );
    private float value; //number of millimeters

    private LengthUnit ( float value ) {
        this.value = value;
    }

    public float between ( Length length1, Length length2 ) {
        float number1 = getNumber ( length1 );
        float number2 = getNumber ( length2 );
        return number2 - number1;

    }

    private float getNumber ( Length length1 ) {
        return length1.getNumber ( ) * ( length1.getUnit ( ).getValue ( ) / this.value );
    }

    public float getValue ( ) {
        return this.value;
    }
}